'use strict';
const cors = require('cors');
const express = require('express');
const logicsFile = require('./logics');
const response = require('./response')
let bodyParser = require('body-parser');
const { json } = require('body-parser');
const app = express();
app.use(cors());

app.use(bodyParser.urlencoded({
    extended: false
}));

app.post('/ajiranet/process', async function (req, res) {
    try {
       
        let data = Object.keys(req.body)[0]
        let data_array = data.split('\n');

        let command = data_array[0];
        let body_data = (data_array.length > 2) ? JSON.parse(data_array[data_array.length - 1]) : null;

        let command_array = command.split(" ")
        let result = await logicsFile.routeToActions(command_array[0], command_array[1], body_data);

        return response.send(result, res)
    } catch (error) {
        console.log("Error at app.js file: ", error);
        return response.error(error.message, res)
    }
});

app.listen(8082, 'localhost', () => {
    console.log(" Node server started and running at 8082 !")
})
