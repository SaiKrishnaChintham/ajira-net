'use strict';
const fs = require('fs');
const graph = require('node-dijkstra')
let dataFile = require('./dataFile.json')
class logicsFile {

    async addDevice(data) {
        try {
            if (dataFile['strengths'][data['name']] === undefined && dataFile['routes'][data['name']] === undefined) {

                dataFile.devices.push(data);
                dataFile['strengths'][data['name']] = 5;
                dataFile['routes'][data['name']] = {};
            }
            fs.writeFileSync('dataFile.json', JSON.stringify(dataFile));
            return { "msg": "Successfully added device" };
        } catch (error) {
            console.log("Error: ", error);
            throw error;
        }
    }

    async modifyStrength(data) {
        try {
            console.log("--- strength -----------", data);
            if (dataFile['strengths'][data['name']] !== undefined) {

                dataFile['strengths'][data['name']] = data['value'];
            }
            fs.writeFileSync('dataFile.json', JSON.stringify(dataFile));
            return { "msg": "Successfully modified device strength" };
        } catch (error) {
            console.log("Error: ", error);
            throw error;
        }
    }

    isSuperset(set, subset) {
        for (let elem of subset) {
            if (!set.has(elem)) {
                return false
            }
        }
        return true
    }
    union(setA, setB) {
        let _union = new Set(setA)
        for (let elem of setB) {
            _union.add(elem)
        }
        return _union
    }

    async createConnection(data) {
        try {
            let oldDevices = new Set(Object.keys(dataFile['routes']))
            let newDevices = new Set(data['targets'])

            if (dataFile['routes'][data['source']] !== undefined && this.isSuperset(oldDevices, newDevices) === true) {

                const oldRoutes = Object.keys(dataFile['routes'][data['source']]).length

                for (let node of data['targets']) {
                    dataFile['routes'][data['source']][node] = 1
                    dataFile['routes'][node][data['source']] = 1
                }
                fs.writeFileSync('dataFile.json', JSON.stringify(dataFile));

                if (oldRoutes === Object.keys(dataFile['routes'][data['source']]).length) {

                    return { "msg": "devices already connected" };
                }
            }
            return { "msg": "Successfully added Connection" };
        } catch (error) {
            console.log("Error: ", error);
            throw error;
        }
    }
    async fetchDevices() {
        try {
            console.log("--- fetch-----------");

            return dataFile['devices']
        } catch (error) {
            console.log("Error: ", error);
            throw error;
        }
    }
    async fetchRoute(cmd) {
        try {
            var query = {};
            let queryString = cmd.split("?")[1];
            var pairs = queryString.split("&")
            
            for (var i = 0; i < pairs.length; i++) {
                var pair = pairs[i].split('=');
                query[pair[0]] = pair[1];
            }

            const route = new graph(dataFile['routes']);
            let pp = route.path(query.from, query.to);
            
        } catch (error) {
            console.log("Error: ", error);
            throw error;
        }
    }

    async routeToActions(cmd_type, cmd, data) {
        try {
            let result = {}
            let input = {
                'cmd_type': cmd_type,
                'cmd': cmd,
                'data': data
            }
            console.log("input = ", input);
            switch (cmd_type) {
                case 'CREATE':
                    switch (cmd) {
                        case '/devices':
                            result = await new logicsFile().addDevice(data);
                            break;
                        case '/connections':
                            result = await new logicsFile().createConnection(data);
                            break;
                    }
                    break;
                case 'MODIFY':
                    data['name'] = cmd.split("/")[2]
                    result = await new logicsFile().modifyStrength(data);
                    break;
                case 'FETCH':

                    switch (cmd) {
                        case '/devices':
                            result = await new logicsFile().fetchDevices();
                            break;
                        default:
                            result = await new logicsFile().fetchRoute(cmd);
                            break;
                    }
                    break;
            }
            return result;
        } catch (error) {
            console.log("Error: ", error);
            return {
                'error': error.message
            }
        }
    }
}
module.exports = new logicsFile();