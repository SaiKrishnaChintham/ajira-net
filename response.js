'use strict';
class Response {

    send(data, res) {
        let resultString = JSON.stringify(data)
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.status(200).end(resultString);
    }
    error(data, res) {
        let resultString = JSON.stringify(data)
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.status(400).end(resultString);
    }
}
module.exports = new Response();